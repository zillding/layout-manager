LayoutManager = React.createClass
	propTypes:
		allowEdit: React.PropTypes.bool
		data: React.PropTypes.array
		defaultFrameContent: React.PropTypes.object
		displayWidthHeightRatio: React.PropTypes.number
		onSave: React.PropTypes.func
	displayName: 'LayoutManager'
	getInitialState: ->
		editing: if @props.allowEdit then true else false
		fullscreen: if @props.allowEdit then false else true
		benchHeight: window.innerHeight
		data: _.map @props.data, (el) ->
			_.extend {}, el,
				_id: _.uniqueId()
	componentDidMount: ->
		lmDomNode = React.findDOMNode @
		radio = @props.displayWidthHeightRatio ? 1
		@setState
			benchHeight: $(lmDomNode).width() / radio
		$(window).resize =>
			@setState
				benchHeight: $(lmDomNode).width() / radio
	_addFrame: ->
		@setState
			data: @state.data.concat
				_id: _.uniqueId()
				type: 'iframe'
				layout: {x:0,y:0,w:3,h:3}
	_removeFrame: (frameKey) ->
		@setState
			data: _.reject @state.data, (frame) ->
				frame._id is frameKey
	_updateFrameData: (frameData) ->
		newData = _.map @state.data, (el) ->
			if frameData._id is el._id
				_.extend {}, el, frameData
			else
				el
		@setState
			data: newData
	_changeLayout: (layout) ->
		newData = _.map @state.data, (frameData) ->
			newLayout = _.find layout, (el) ->
				el.i is frameData._id
			_.extend {}, frameData,
				layout: _.pick newLayout, 'x', 'y', 'w', 'h'
		@setState
			data: newData
	_saveLayout: ->
		# callback
		@props.onSave _.map @state.data, (el) ->
			_.omit el, '_id'
	_toggleEdit: ->
		@setState
			editing: not @state.editing
	_toggleFullscreen: ->
		@setState
			fullscreen: not @state.fullscreen
	render: ->
		controlPanelProps =
			editing: @state.editing
			fullscreen: @state.fullscreen
			toggleEdit: @_toggleEdit
		layoutBenchProps =
			editing: @state.editing
			height: if @state.fullscreen then window.innerHeight else @state.benchHeight
			defaultFrameContent: @props.defaultFrameContent
			data: @state.data
			changeLayout: @_changeLayout
		if @state.editing
			_.extend controlPanelProps,
				addFrame: @_addFrame
				saveLayout: @_saveLayout
				toggleFullscreen: @_toggleFullscreen
			_.extend layoutBenchProps,
				removeFrame: @_removeFrame
				updateFrameData: @_updateFrameData

		layoutManagerStyle =
			height: if @state.fullscreen then '100%' else @state.benchHeight
			overflow: 'hidden'
		lmClass = 'layout-manager'
		lmClass += ' editing' if @state.editing
		lmClass += ' fullscreen' if @state.fullscreen
		if @props.allowEdit
			<div className={lmClass}
				style={layoutManagerStyle}>
				<ControlPanel {...controlPanelProps} />
				<LayoutBench {...layoutBenchProps} />
			</div>
		else
			<div className={lmClass}
				style={layoutManagerStyle}>
				<LayoutBench {...layoutBenchProps} />
			</div>

ControlPanel = React.createClass
	propTypes:
		editing: React.PropTypes.bool.isRequired
		fullscreen: React.PropTypes.bool.isRequired
		addFrame: React.PropTypes.func
		toggleEdit: React.PropTypes.func
		toggleFullscreen: React.PropTypes.func
		saveLayout: React.PropTypes.func
	displayName: 'ControlPanel'
	_createButton: (sGlyph, sTitle, fClickHandler, sBsStyle) ->
		<ReactBootstrap.OverlayTrigger
			placement="bottom"
			overlay={<ReactBootstrap.Tooltip>{sTitle}</ReactBootstrap.Tooltip>}>
			<ReactBootstrap.Button bsStyle={sBsStyle} onClick={fClickHandler}>
				<ReactBootstrap.Glyphicon glyph={sGlyph} />
			</ReactBootstrap.Button>
		</ReactBootstrap.OverlayTrigger>
	render: ->
		if @props.editing
			# in editing state, show the full control panel
			sFullscreenGlyph = if @props.fullscreen then 'resize-small' else 'fullscreen'
			panelStyle =
				width: '220px'
				position: 'absolute'
				top: '10px'
				right: '10px'
				zIndex: '1'
			glyphStyle =
				fontSize: '20px'
				cursor: 'move'
			<ReactDraggable
				zIndex={9999}
				bounds="parent"
				handle=".drag-handle">
				<ReactBootstrap.Well bsSize="small" style={panelStyle}>
					<ReactBootstrap.ButtonToolbar>
						{@_createButton 'plus', 'Add', @props.addFrame, 'success'}
						{@_createButton 'eye-open', 'Preview', @props.toggleEdit}
						{@_createButton sFullscreenGlyph, 'Toggle Fullscreen', @props.toggleFullscreen}
						{@_createButton 'save', 'Save', @props.saveLayout}
						<ReactBootstrap.Glyphicon
							glyph="move"
							className="pull-right drag-handle"
							style={glyphStyle}>
						</ReactBootstrap.Glyphicon>
					</ReactBootstrap.ButtonToolbar>
				</ReactBootstrap.Well>
			</ReactDraggable>
		else
			# in viewing state, minify the control panel
			miniStyle =
				position: 'absolute'
				top: '10px'
				right: '10px'
				cursor: 'pointer'
				zIndex: '9999'
			<ReactBootstrap.Glyphicon
				glyph="edit"
				className="mini-control"
				style={miniStyle}
				onClick={@props.toggleEdit}>
			</ReactBootstrap.Glyphicon>

LayoutBench = React.createClass
	propTypes:
		changeLayout: React.PropTypes.func
		data: React.PropTypes.array
		defaultFrameContent: React.PropTypes.object
		editing: React.PropTypes.bool.isRequired
		height: React.PropTypes.number
		removeFrame: React.PropTypes.func
		updateFrameData: React.PropTypes.func
	displayName: 'LayoutBench'
	_createElement: (el) ->
		gridItemProps =
			x: el.layout.x
			y: el.layout.y
			w: el.layout.w
			h: el.layout.h
			maxW: 24
			maxH: 24
		<div key={el._id} _grid={gridItemProps} style={{overflow: 'hidden'}}>
			<Frame {...@props} data={el} />
		</div>
	render: ->
		rglProps =
			autoSize: false
			className: 'layout'
			cols: 24
			draggableHandle: '.drag-handle'
			initialWidth: $(window).width()
			margin: [0, 0]
		<ReactGridLayout
			onLayoutChange={@props.changeLayout}
			isDraggable={@props.editing}
			isResizable={@props.editing}
			rowHeight={@props.height/24}
			{...rglProps}>
			{_.map(@props.data, @_createElement)}
		</ReactGridLayout>

Frame = React.createClass
	propTypes:
		data: React.PropTypes.object.isRequired
		defaultFrameContent: React.PropTypes.object
		editing: React.PropTypes.bool.isRequired
		removeFrame: React.PropTypes.func
		updateFrameData: React.PropTypes.func
	displayName: 'Frame'
	statics:
		_isValidUrl: (url) ->
			/^https?:\/\/.+$/i.test url
	render: ->
		if @props.editing
			<EditFrame {...@props} />
		else
			<ViewFrame default={@props.defaultFrameContent} {...@props.data} />

EditFrame = React.createClass
	propTypes:
		data: React.PropTypes.object.isRequired
		defaultFrameContent: React.PropTypes.object
		removeFrame: React.PropTypes.func
		updateFrameData: React.PropTypes.func
	displayName: 'EditFrame'
	statics:
		_getIframeInputBsStyle: (url) ->
			if _.isEmpty url
				return 'warning'
			if Frame._isValidUrl url
				'success'
			else
				'error'
	_removeFrame: ->
		@props.removeFrame @props.data._id
	_updateType: (key) ->
		@props.updateFrameData
			_id: @props.data._id
			type: key
	_changeUrl: ->
		url = @refs.iframeInput.getValue().trim()
		@props.updateFrameData
			_id: @props.data._id
			iframe:
				src: url
	_changeImg: ->
		file = @refs.imgInput.getInputDOMNode().files[0]
		# Only process image files.
		if file.type.match 'image.*'
			reader = new FileReader()
			# Closure to capture the file information.
			reader.onload = ((theFile) =>
				(e) =>
					@props.updateFrameData
						_id: @props.data._id
						img:
							name: theFile.name
							src: e.target.result
			) file
			# Read in the image file as a data URL.
			reader.readAsDataURL file
	_changeVideo: ->
		URL = window.URL ? window.webkitURL
		file = @refs.videoInput.getInputDOMNode().files[0]
		fileUrl = URL.createObjectURL file
		@props.updateFrameData
			_id: @props.data._id
			video:
				name: file.name
				src: fileUrl
	render: ->
		frameStyle =
			padding: '5px'
		closeButtonStyle =
			marginLeft: '5px'
		progressBarStyle =
			marginBottom: '3px'
			cursor: 'move'
		previewLabelStyle =
			position: 'absolute'
			opacity: '0.8'
		labelStyle =
			position: 'absolute'
			bottom: '0px'
		<div style={frameStyle}>
			<button
				type="button"
				className="close"
				aria-label="Close"
				style={closeButtonStyle}
				onClick={@_removeFrame}>
				<span aria-hidden="true">&times;</span>
			</button>
			<ReactBootstrap.ProgressBar
				now={100}
				className="drag-handle"
				style={progressBarStyle}>
			</ReactBootstrap.ProgressBar>
			<ReactBootstrap.TabbedArea
				justified
				activeKey={@props.data.type}
				animation={false}
				onSelect={@_updateType}>
				<ReactBootstrap.TabPane eventKey={'iframe'} tab="Iframe">
					<ReactBootstrap.Input
						ref="iframeInput"
						type="text"
						label="URL"
						help="Please provide a valid url"
						placeholder="Enter URL"
						bsStyle={EditFrame._getIframeInputBsStyle @props.data.iframe?.src}
						hasFeedback
						value={@props.data.iframe?.src}
						onChange={@_changeUrl}>
					</ReactBootstrap.Input>
				</ReactBootstrap.TabPane>
				<ReactBootstrap.TabPane eventKey={'img'} tab="Image">
					<ReactBootstrap.Input
						ref="imgInput"
						type="file"
						label="Image File"
						help="Choose an image file"
						accept={'image/*'}
						onChange={@_changeImg}>
					</ReactBootstrap.Input>
				</ReactBootstrap.TabPane>
				<ReactBootstrap.TabPane eventKey={'video'} tab="Video">
					<ReactBootstrap.Input
						ref="videoInput"
						type="file"
						label="Video File"
						help="Choose a video file with format *.mp4, *.webm, *.ogg"
						accept={'video/*'}
						onChange={@_changeVideo}>
					</ReactBootstrap.Input>
				</ReactBootstrap.TabPane>
			</ReactBootstrap.TabbedArea>
			<ReactBootstrap.Well bsSize="small">
				<h3 style={previewLabelStyle}>
					<ReactBootstrap.Label>Preview</ReactBootstrap.Label>
				</h3>
				<ViewFrame default={@props.defaultFrameContent} {...@props.data} />
			</ReactBootstrap.Well>
			<h4 style={labelStyle}>
				<ReactBootstrap.Label
					bsStyle="info"
					style={{marginRight: '5px'}}>
					size: {@props.data.layout?.w} x {@props.data.layout?.h}
				</ReactBootstrap.Label>
				<ReactBootstrap.Label
					bsStyle="info">
					position: ({@props.data.layout?.x}, {@props.data.layout?.y})
				</ReactBootstrap.Label>
			</h4>
		</div>

ViewFrame = React.createClass
	propTypes:
		default: React.PropTypes.object
		iframe: React.PropTypes.object
		img: React.PropTypes.object
		type: React.PropTypes.oneOf ['iframe', 'img', 'video']
		video: React.PropTypes.object
	displayName: 'ViewFrame'
	getInitialState: ->
		showDefaultFrame: true
	getDefaultProps: ->
		frameStyle:
			height: '100%'
			width: '100%'
	componentDidMount: ->
		@_addFrameListener()
	componentDidUpdate: (prevProps, prevState) ->
		if @props.type isnt prevProps.type
			@_addFrameListener()
			@setState
				showDefaultFrame: true
	_createFrame: (frameData) ->
		if frameData
			switch frameData.type
				when 'iframe'
					<iframe src={frameData.iframe?.src} style={@props.frameStyle} />
				when 'img'
					<img src={frameData.img?.src} style={@props.frameStyle} />
				when 'video'
					<video src={frameData.video?.src} style={@props.frameStyle} controls />
				else
					<p>Unknown type</p>
		else
			undefined
	_addFrameListener: ->
		actualFrameContent = $(React.findDOMNode @).children('.actual').children().get 0
		switch @props.type
			when 'iframe'
				actualFrameContent.onload = =>
					@setState
						showDefaultFrame: false
			when 'img'
				actualFrameContent.onload = =>
					@setState
						showDefaultFrame: false
			when 'video'
				actualFrameContent.onloadstart = =>
					@setState
						showDefaultFrame: false
	_videoLoaded: ->
		console.log 'video loaded'
	render: ->
		hiddenFrameStyle = _.extend
			display: 'none'
		, @props.frameStyle
		defaultFrameStyle = if @state.showDefaultFrame then @props.frameStyle else hiddenFrameStyle
		actualFrameStyle = if @state.showDefaultFrame then hiddenFrameStyle else @props.frameStyle
		<div style={@props.frameStyle}>
			<div className="default" style={defaultFrameStyle}>
				{@_createFrame @props.default}
			</div>
			<div className="actual" style={actualFrameStyle}>
				{@_createFrame @props}
			</div>
		</div>
